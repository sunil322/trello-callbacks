const boards = require('./boards.json');
const callback1 = require('./callback1.cjs');
const callback2 = require('./callback2.cjs');
const callback3 = require('./callback3.cjs');

function callback6(boardName, callbackFunction) {
    setTimeout(() => {
        if (typeof boardName === 'string') {
            let boardInfo = boards.find((board) => {
                return board.name === boardName;
            });
            if (boardInfo !== undefined) {
                callback1(boardInfo.id, (error, boardInfo) => {
                    if (error) {
                        callbackFunction(error);
                    } else {
                        console.log(boardInfo);
                        callback2(boardInfo.id, (error, listofBoards) => {
                            console.log({ [boardInfo.id]: listofBoards });
                            if (error) {
                                callbackFunction(error);
                            } else {
                                listofBoards.forEach((list) => {
                                    callback3(list.id, (error, cardsInfo) => {
                                        if (error) {
                                            callbackFunction(error);
                                        } else {
                                            console.log({ [list.id]: cardsInfo });
                                        }
                                    });
                                });
                            }
                        });
                    }
                });
            } else {
                console.log("No boards found with this name " + boardName);
            }
        } else {
            console.log("Board name must be string");
        }
    }, 2 * 1000);
}

module.exports = callback6;