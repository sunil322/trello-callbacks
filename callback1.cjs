const boards = require('./boards.json');

function callback1(boardId, callbackFunction) {
    setTimeout(() => {
        if (typeof boardId === 'string' && typeof callbackFunction === 'function') {
            let boardInfo = boards.find((board) => {
                return board.id === boardId;
            });
            if (boardInfo === undefined) {
                callbackFunction("No board info found with this id " + boardId);
            } else {
                callbackFunction(null, boardInfo);
            }
        } else {
            console.log("Board id and callback function expected in arguments");
        }
    }, 2 * 1000);
}

module.exports = callback1;