const lists = require('./lists.json');

function callback2(boardId, callbackFunction) {
    setTimeout(() => {
        if (typeof boardId === 'string' && typeof callbackFunction === 'function') {
            if (lists[boardId] === undefined) {
                callbackFunction("No lists found with this board id " + boardId);
            } else {
                callbackFunction(null, lists[boardId]);
            }
        }else{
            console.log("Board id and callback function expected in arguments");
        }
    }, 2 * 1000);
}

module.exports = callback2;