const boards = require('./boards.json');
const callback1 = require('./callback1.cjs');
const callback2 = require('./callback2.cjs');
const callback3 = require('./callback3.cjs');

function callback4(boardName,callbackFunction) {
    setTimeout(() => {
        if (typeof boardName === 'string') {
            let boardInfo = boards.find((board) => {
                return board.name === boardName;
            });
            if(boardInfo !== undefined){
                callback1(boardInfo.id, (error, boardInfo) => {
                    if (error) {
                        callbackFunction(error);
                    } else {
                        console.log(boardInfo);
                        callback2(boardInfo.id, (error, listofBoards) => {
                            if (error) {
                                callbackFunction(error);
                            } else {
                                console.log({[boardInfo.id]:listofBoards});
                                let mindlistInfo = listofBoards.find((list)=>{
                                    return list.name === 'Mind';
                                });
                                if(mindlistInfo !== undefined){
                                    callback3(mindlistInfo.id,(error,cardsOfMind)=>{
                                        if(error){
                                            callbackFunction(error);
                                        }else{
                                            console.log({[mindlistInfo.id]:cardsOfMind});
                                        }
                                    })
                                }else{
                                    console.log("No list found for mind");
                                }
                            }
                        });
                    }
                });
            }else{
                console.log("No boards found with this name "+boardName);
            }
        } else {
            console.log("Board name must be string");
        }
    }, 2 * 1000);
}

module.exports = callback4;