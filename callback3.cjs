const cards = require('./cards.json');

function callback3(listId, callbackFunction) {
    setTimeout(() => {
        if (typeof listId === 'string' && typeof callbackFunction === 'function') {
            if (cards[listId] === undefined) {
                callbackFunction("No card found with this list id " + listId);
            } else {
                callbackFunction(null, cards[listId]);
            }
        }else{
            console.log("List id and callback function expected in arguments");
        }
    }, 2 * 1000);
}

module.exports = callback3;
